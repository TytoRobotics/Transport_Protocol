sleep 3
while true
do
python ~/Transport_Protocol/UDP/Python/RCB_pose_forward.py
echo "No internet, Relaunching the script"
sleep 1
done
# If you are using multiple drones, replace line 4 with this:
# python ~/Transport_Protocol/UDP/Python/RCB_pose_forward.py --controller_id 4X4X4X4X
# Where 4X4X4X4X is the 8 character alphanumeric string uniquely identifying your 
# controller ID. You can find your controller ID under the Otus tracker, or
# in RCbenchmark tracking lab.