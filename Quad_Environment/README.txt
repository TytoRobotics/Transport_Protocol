Following are the steps to install all the required dependecies on a new Raspian system.
1. Check out to the branch you want to build after cloning the Transport Protocol repo.
2. Navigate to the Quad_Environment folder. 
3a. Run the following command in the terminal "bash installer_part1.sh".
The command bash is important!

The installer file will automatically install all the requirements and reboot the quad. 

# To launch the simulation
Install Jmavsim on your desktop computer.
Launch Jmavsim with java -Djava.ext.dirs= -jar jmavsim_run.jar -tcp 192.168.1.116:4570 -lockstep
Launch px4_sitl_default on the Raspberry Pi with:
cd ~/src/Firmware
make px4_sitl_default none

Launch mavlink-routerd with:
mavlink-routerd -t 4560 -p 192.168.1.106:4570

To make a copy of your SD card during development, follow this process:

1. Insert a USB SD card reader in the Raspberry Pi with an SD card.
2. In a console, type 
    sudo rpi-clone sda
    Wait until the copy it done. You can now test the copy.
3. (Optionnal, to reduce memory for storage) With the copied SD card in the SD card reader, launch gparted on raspbian and reduce the partition size of the SDA partition
4. Remove the SD card reader and connect it to Widows.
5. Use the software win32 imager to make a backup of your sd card.
6. (Optionnal) Zip the file.

To restore the copy

1. Format an SD card with the software [SD Memory Card Formatter] (https://www.sdcard.org/downloads/formatter/)
2. Use win32 imager to write to the SD card.
