#!/bin/bash
    while true;
    do 
        echo -------------------------------------------------------------
        declare -i err
        err=0
        echo Cloning RCBenchmark repositories and changing working directory.
        cd
        cd /home/pi/Transport_Protocol
        err=err+$?
        sudo git pull
        err=err+$?
        echo -------------------------------------------------------------
        
        echo Creating auto launching scripts
        cd
        mkdir autolaunch_scripts
        cd
        (crontab -l ; echo "@reboot screen -d -S mavproxy -m -L /home/pi/autolaunch_scripts/mavproxy.log sh /home/pi/autolaunch_scripts/launch_mavproxy_rcb.sh") | sort - | uniq - | crontab -
        err=err+$?
    	sleep 0.1
    	(crontab -l ; echo "@reboot screen -d -S poseforward -m -L /home/pi/autolaunch_scripts/pose.log sh /home/pi/autolaunch_scripts/launch_pose_forward_rcb.sh") | sort - | uniq - | crontab -
        err=err+$?
    	sleep 0.1
    	(crontab -l ; echo "@reboot screen -d -S OLED -m sh /home/pi/autolaunch_scripts/launch_OLED_screen.sh") | sort - | uniq - | crontab -
        err=err+$?
    	sleep 0.1
        (crontab -l ; echo "@reboot screen -d -S C9 -m sh /home/pi/autolaunch_scripts/launch_c9.sh") | sort - | uniq - | crontab -
        echo Setting up autolaunch_scripts folder
        cp /home/pi/Transport_Protocol/Quad_Environment/launch_mavproxy_rcb.sh /home/pi/autolaunch_scripts
        err=err+$?
	    cp /home/pi/Transport_Protocol/Quad_Environment/launch_pose_forward_rcb.sh /home/pi/autolaunch_scripts
	    err=err+$?
        cp /home/pi/Transport_Protocol/Quad_Environment/launch_OLED_screen.sh /home/pi/autolaunch_scripts
        err=err+$?
	    cp /home/pi/Transport_Protocol/Quad_Environment/launch_c9.sh /home/pi/autolaunch_scripts
        err=err+$?
    	echo Setup Complete! 
        sleep 0.2
        
        if [ $err -gt 0 ];
    	then
    	    echo Forward script setup installation failed.
    	    break
    	fi
    	echo -------------------------------------------------------------
    	
    	echo Making Files executable
        sudo chmod +x ~/autolaunch_scripts/launch_mavproxy_rcb.sh
        err=err+$?
    	sudo chmod +x ~/autolaunch_scripts/launch_pose_forward_rcb.sh
        sudo chmod +x ~/autolaunch_scripts/launch_OLED_screen.sh
        err=err+$?
    	sudo chmod +x ~/autolaunch_scripts/launch_c9.sh
        err=err+$?
    	echo Creating log deleting scripts
        sudo sed -i '$i \rm ~/autolaunch_scripts/pose.log' /etc/rc.local
        err=err+$?
    	sudo sed -i '$i \rm ~/autolaunch_scripts/mavproxy.log' /etc/rc.local
    	
        cd && sudo apt-get -y install curl
	sudo apt-get -y install libcurl4-openssl-dev
	err=err+$?
	echo ----------------------------------------------------------------
    echo Cloning and Installing DronecodeSDK
    cd && git clone https://github.com/charles-blouin/DronecodeSDK
    err=err+$?
    git checkout master
    err=err+$?
	cd DronecodeSDK
    err=err+$?
	git submodule update --init --recursive
    err=err+$?
    make clean
	make default
  	err=err+$?
	sudo make default install
        err=err+$?
    	sudo make default install
        err=err+$?
    	cd && sudo ldconfig
    	err=err+$? 
    	echo Dronecode cloned and installed.
        echo -------------------------------------------------------------
        echo Installing Cmake
        sudo apt-get -y install cmake 
        err=err+$?
    	if [ $err -gt 0 ];
    	then
    	    echo DronecodeSDK installation failed
    	    break
    	fi
    	echo -------------------------------------------------------------
        echo All dependencies installed
        echo -------------------------------------------------------------
        
        cd
        wget https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_nuttx.sh
        sudo bash ubuntu_sim_nuttx.sh
        
        cd
        echo "sudo nano /etc/wpa_supplicant/wpa_supplicant.conf" > /home/pi/change_wifi.sh
        err=err+$?
        sudo chmod +x change_wifi.sh
        err=err+$?
        if [ $err -gt 0 ];
    	then
    	    echo Error creating the change_wifi.sh file
    	    break
    	fi
    	echo -------------------------------------------------------------
        echo change_wifi.sh file created
        echo -------------------------------------------------------------
        
        
        echo "Part 2/ Part 2 complete. Complete environment Setup on:  $(date)" > /home/pi/Created_on.txt
        echo "Part 2 / Part 2 is now complete."
        echo Preparing to reboot 
        echo you have 5 seconds to cancel. to cancel press Ctl+C
        sleep 5
        echo Rebooting Now
        sleep 1
        sudo reboot
	done
        
