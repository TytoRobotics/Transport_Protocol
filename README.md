## Transport Protocol

Version: v0.0.3 2018-12-21

## Synopsis

### UDP Clients

The files in the UDP folder are intended to be used as a reference when developping a tracked robot using the RCbenchmark Tracking Lab Software as the Server. See [the documentation](https://docs.rcbenchmark.com/en/motion-capture/otus-tracker.html) for more information.  We support the following languages on both Linux/Windows machines:

- C
- C++
- C#
- Python
- MATLAB
- LabVIEW

### Quadcopter Environment

The Quad_Environment folder contains all the files to build the [Otus Quadcopter](https://www.rcbenchmark.com/pages/otus-quadcopter). They can also be used as a reference for any platform using motion capture, the PX4 and a companion computer. See [the documentation](https://docs.rcbenchmark.com/en/motion-capture/introduction.html) for more information.
Architecture:

![Architecture](https://storage.googleapis.com/cdn-docs.rcbenchmark.com/images/mocap/otus-quadcopter-01/Otus%20Quadcopter%20Software%20Architecture.svg)

If you are building your own platform, you will need to change some [parameters](https://docs.rcbenchmark.com/en/motion-capture/software-setup/hui.html#4135-parameter-setup-for-motion-capture) of the PX4 firmare with QGroudControl.


## Cloning Projects

To clone this project, use 

git clone https://gitlab.com/TytoRobotics/Transport_Protocol.git

This package is compatible with:

- rcbenchmark_ros_pkg v0.0.2 : git clone https://gitlab.com/TytoRobotics/rcbenchmark_ros_pkg.git
- RCbenchmark Tracking lab : https://docs.rcbenchmark.com/en/motion-capture/rcbenchmark-tracking-lab.html


## License

Author: Nemanja Babic <nemanja.babic@hotmail.com>, Charles Blouin <charles.blouin@rcbenchmark>, Salauddin Ali Ahmed

Copyright 2017 Tyto Robotics Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.