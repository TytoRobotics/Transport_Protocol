// client_udp.h

#ifndef CLIENT_UDP_H
#define CLIENT_UDP_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdexcept>

bool checkNativeEndianness();

class udp_runtime_error : public std::runtime_error
{
public:
  udp_runtime_error(const char *w) : std::runtime_error(w) {}
};

class udp_client
{	
public:
	udp_client(const std::string& addr, int port); // constructor
	~udp_client(); //destructor

	int	get_socket() const; // get socket
  int get_port() const; // get port
  std::string get_addr() const; //get address
    
  int recv(char *msg, size_t max_size); // receive data

private:
  int _socket;
  int _port;
  std::string _addr;
  struct addrinfo * _addrinfo;
};
#endif
// CLIENT_UDP_H
