/* ========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Linux Socket Programming (UDP) - C++
===========================================
Description: UDP Client
=========================================== */

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include "client_udp.h"

#define BUFSIZE 184
#define PORT 5400

int main(int argc, char *argv[]){

	// static_assert( sizeof(double) == 8 ); // debug - compile time check

	bool little_endian = checkNativeEndianness(); // check endianness

	// create udp socket by passing IP address and port #
	udp_client _socket("127.0.0.1", PORT);
	
	// initialize buffer
	char buffer[BUFSIZE];

	double out[BUFSIZE/8];

	while(true){

        std::cout << "Waiting for datagram ..." << std::endl;

        // receive packet and send it to buffer
        // return value is size of packet in bytes
        int i = _socket.recv(buffer, BUFSIZE);

        if(i == BUFSIZE){
            // success
            if(little_endian){

            // little endian system

            // copy content of buffer in address of out array
            memcpy(&out, buffer, sizeof out);

            // convert binary array to ascii string format
            std::string strID(reinterpret_cast<char*>(&out[0]));
            strID.erase (strID.begin()+8, strID.end());

            // print results
            std::cout << "id = " << strID << std::endl;
            std::cout << "timestamp = " << out[1] << std::endl;
            std::cout << "lin_pos_x = " << out[2] << std::endl;
            std::cout << "lin_pos_y = " << out[3] << std::endl;
            std::cout << "lin_pos_z = " << out[4] << std::endl;
            std::cout << "lin_vel_x = " << out[5] << std::endl;
            std::cout << "lin_vel_y = " << out[6] << std::endl;
            std::cout << "lin_vel_z = " << out[7] << std::endl;
            std::cout << "lin_acc_x = " << out[8] << std::endl;
            std::cout << "lin_acc_y = " << out[9] << std::endl;
            std::cout << "lin_acc_z = " << out[10] << std::endl;
            std::cout << "quaternion_x = " << out[11] << std::endl;
            std::cout << "quaternion_y = " << out[12] << std::endl;
            std::cout << "quaternion_z = " << out[13] << std::endl;
            std::cout << "quaternion_w = " << out[14] << std::endl;
            std::cout << "ang_vel_x = " <<out[15] << std::endl;
            std::cout << "ang_vel_y = " <<out[16] << std::endl;
            std::cout << "ang_vel_z = " <<out[17] << std::endl;
            std::cout << "ang_acc_x = " <<out[18] << std::endl;
            std::cout << "ang_acc_y = " <<out[19] << std::endl;
            std::cout << "ang_acc_z = " <<out[20] << std::endl;
            std::cout << "button_1 = " <<out[21] << std::endl;
            std::cout << "button_2 = " <<out[22] << std::endl;
            std::cout << "Received datagram.\n" << std::endl;

        }else{ 

            // big endian system

            // swap byte-order for big-endian system
            for (int i=0; i<(sizeof out)/2; i++)
            {
                char temp = buffer[i];
                buffer[i] = buffer[(sizeof out)-1-i];
                buffer[(sizeof buffer)-1-i] = temp;
            }

            // copy content of buffer in address of out array
            memcpy(&out, buffer, sizeof out);

            // convert binary array to ascii string format
            std::string strID(reinterpret_cast<char*>(&out[0]));
            strID.erase (strID.begin()+8, strID.end());

            // print results
            std::cout << "id = " << strID << std::endl;
            std::cout << "timestamp = " << out[1] << std::endl;
            std::cout << "lin_pos_x = " << out[2] << std::endl;
            std::cout << "lin_pos_y = " << out[3] << std::endl;
            std::cout << "lin_pos_z = " << out[4] << std::endl;
            std::cout << "lin_vel_x = " << out[5] << std::endl;
            std::cout << "lin_vel_y = " << out[6] << std::endl;
            std::cout << "lin_vel_z = " << out[7] << std::endl;
            std::cout << "lin_acc_x = " << out[8] << std::endl;
            std::cout << "lin_acc_y = " << out[9] << std::endl;
            std::cout << "lin_acc_z = " << out[10] << std::endl;
            std::cout << "quaternion_x = " << out[11] << std::endl;
            std::cout << "quaternion_y = " << out[12] << std::endl;
            std::cout << "quaternion_z = " << out[13] << std::endl;
            std::cout << "quaternion_w = " << out[14] << std::endl;
            std::cout << "ang_vel_x = " <<out[15] << std::endl;
            std::cout << "ang_vel_y = " <<out[16] << std::endl;
            std::cout << "ang_vel_z = " <<out[17] << std::endl;
            std::cout << "ang_acc_x = " <<out[18] << std::endl;
            std::cout << "ang_acc_y = " <<out[19] << std::endl;
            std::cout << "ang_acc_z = " <<out[20] << std::endl;
            std::cout << "button_1 = " <<out[21] << std::endl;
            std::cout << "button_2 = " <<out[22] << std::endl;
            std::cout << "Received datagram.\n" << std::endl;
        }
        }else{
            // failed. missing bytes.
        }
	}
	return 0;
}

udp_client::udp_client(const std::string& addr, int port): _port(port), _addr(addr)
{
    
    char decimal_port[16];

    // Write formatted output to sized buffer
    snprintf(decimal_port, sizeof(decimal_port), "%d", _port);
    decimal_port[sizeof(decimal_port) / sizeof(decimal_port[0]) - 1] = '\0';
   
   	// struct that holds host address information.
    struct addrinfo hints;

    // Clears hints struct 
    memset(&hints, 0, sizeof(hints));

    /* Allow IPv4 or IPv6 */
    hints.ai_family = AF_UNSPEC;

    /* Datagram socket */
    hints.ai_socktype = SOCK_DGRAM;

    /* IP Protocol UDP */
    hints.ai_protocol = IPPROTO_UDP;


    int r(getaddrinfo(addr.c_str(), decimal_port, &hints, &_addrinfo));

    if(r != 0 || _addrinfo == NULL)
    {
        throw udp_runtime_error(("invalid address or port for UDP socket: \"" + addr + ":" + decimal_port + "\"").c_str());
    }

    // create UDP socket
    _socket = socket(_addrinfo->ai_family, SOCK_DGRAM | SOCK_CLOEXEC, IPPROTO_UDP);

    if(_socket == -1)
    {
        freeaddrinfo(_addrinfo);
        throw udp_runtime_error(("could not create UDP socket for: \"" + addr + ":" + decimal_port + "\"").c_str());
    }

    // assigns the address specified by ai_addr to the socket referred to by the file descriptor _socket
    // brief: assigning a name to a socket
    r = bind(_socket, _addrinfo->ai_addr, _addrinfo->ai_addrlen);

    // on success binding zero is returned
    if(r != 0)
    {
        freeaddrinfo(_addrinfo);
        close(_socket);
        throw udp_runtime_error(("could not bind UDP socket with: \"" + addr + ":" + decimal_port + "\"").c_str());
    }
}

// Clean up the UDP client.
// This function frees the address info structures and close the socket.
udp_client::~udp_client()
{
    freeaddrinfo(_addrinfo);
    close(_socket);
}

// The socket used by UDP client.
// This function returns the socket identifier.
int udp_client::get_socket() const
{
    return _socket;
}

// The port used by this UDP server.
// This function returns the port attached to the UDP client.
int udp_client::get_port() const
{
    return _port;
}

// Return the address of this UDP client.
// This function returns a verbatim copy of the address
std::string udp_client::get_addr() const
{
    return _addr;
}

/* Wait on a message.
 *
 * This function waits until a message is received on this UDP client.
 *
 * \param[in] msg  The buffer where the message is saved.
 * \param[in] max_size  The maximum size the message.
 *
 */
int udp_client::recv(char *msg, size_t max_size)
{
    return ::recv(_socket, msg, max_size, 0);
}

// Verify Endianness
bool checkNativeEndianness(){
	unsigned short a=0x1234;
	if (*((unsigned char *)&a)==0x12)
		return BIG_ENDIAN;
	else
		return LITTLE_ENDIAN;
}