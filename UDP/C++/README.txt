===========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Socket Programming (UDP) C++:
===========================================

Socket Programming User Datagram Protocol (UDP)

	-Server & Client Concept.

	-Major steps.

		-Client:
			=> Create a socket with the socket()
			=> Send and receive data, use the recv() system calls

	-Client code: client_udp.cpp

	(Linux) compile c++ code	:	$ g++ <file_name>.cpp -o <file_name>.out
	(Linux) run c++ code		:	$ ./<file_name>.out <arg1> <arg2> ...
	
	(Windows) compile c++ code  :	> cl /EHsc <file_name>.cpp
	(Windows) run c++ code		:	> <file_name>.exe