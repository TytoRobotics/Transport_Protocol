/* ========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Windows Socket Programming (UDP) - C++
===========================================
Description: UDP Client
=========================================== */

#include <iostream>
#include <winsock2.h>
#include <string>

#pragma comment(lib, "Ws2_32.lib")
 
using namespace std;

#define PORT 5400
#define BUFSIZE 184

// Verify Endianness
bool checkNativeEndianness(){
	unsigned short a=0x1234;
	if (*((unsigned char *)&a)==0x12)
		return false; // big endian
	else
		return true; // little endian
}
 
int main()
{
    
    char buffer[BUFSIZE];

    int recv_len;

    double out[23];

    bool little_endian = checkNativeEndianness(); // check endianness

    WSADATA WSAData;
 
    SOCKET _socket;
 
    SOCKADDR_IN _socketAddr, _sourceAddr;

    int _sourceAddrSize = sizeof (_sourceAddr);
 
    WSAStartup(MAKEWORD(2,0), &WSAData);
    _socket = socket(AF_INET, SOCK_DGRAM, 0);
 
    _socketAddr.sin_addr.s_addr = INADDR_ANY;
    _socketAddr.sin_family = AF_INET;
    _socketAddr.sin_port = htons(PORT);
 
    bind(_socket, (SOCKADDR *)&_socketAddr, sizeof(_socketAddr));
    listen(_socket, 0);

    while(true)
    {
    	cout << "Waiting for datagram ..." << endl;
    
    	//try to receive some data, this is a blocking call
        if ((recv_len = recvfrom(_socket, buffer, BUFSIZE, 0, (struct sockaddr *) &_sourceAddr, &_sourceAddrSize)) == SOCKET_ERROR)
        {
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            exit(EXIT_FAILURE);
        }

    	if(recv_len == BUFSIZE){
			// success
			
			if(little_endian){
		
				// little endian system
			
				// copy content of buffer in address of out array
				memcpy(&out, buffer, sizeof out);
			
				// convert binary array to ascii string format
                std::string strID(reinterpret_cast<char*>(&out[0]));
                strID.erase (strID.begin()+8, strID.end());

                // print results
                std::cout << "id = " << strID << std::endl;
				std::cout << "timestamp = " << out[1] << std::endl;
				std::cout << "lin_pos_x = " << out[2] << std::endl;
				std::cout << "lin_pos_y = " << out[3] << std::endl;
				std::cout << "lin_pos_z = " << out[4] << std::endl;
				std::cout << "lin_vel_x = " << out[5] << std::endl;
				std::cout << "lin_vel_y = " << out[6] << std::endl;
				std::cout << "lin_vel_z = " << out[7] << std::endl;
				std::cout << "lin_acc_x = " << out[8] << std::endl;
				std::cout << "lin_acc_y = " << out[9] << std::endl;
				std::cout << "lin_acc_z = " << out[10] << std::endl;
				std::cout << "quaternion_x = " << out[11] << std::endl;
				std::cout << "quaternion_y = " << out[12] << std::endl;
				std::cout << "quaternion_z = " << out[13] << std::endl;
				std::cout << "quaternion_w = " << out[14] << std::endl;
				std::cout << "ang_vel_x = " <<out[15] << std::endl;
				std::cout << "ang_vel_y = " <<out[16] << std::endl;
				std::cout << "ang_vel_z = " <<out[17] << std::endl;
				std::cout << "ang_acc_x = " <<out[18] << std::endl;
				std::cout << "ang_acc_y = " <<out[19] << std::endl;
				std::cout << "ang_acc_z = " <<out[20] << std::endl;
				std::cout << "button_1 = " << out[21] << std::endl;
				std::cout << "button_2 = " << out[22] << std::endl;
				std::cout << "Received datagram.\n" << std::endl;
		
			}else{ 

				// big endian system
				
				// swap byte-order for big-endian system
				for (int i=0; i<(sizeof out)/2; i++)
		    	{
		        	char temp = buffer[i];
		        	buffer[i] = buffer[(sizeof out)-1-i];
		        	buffer[(sizeof buffer)-1-i] = temp;
		    	}

				// copy content of buffer in address of out array
				memcpy(&out, buffer, sizeof out);
				
				// convert binary array to ascii string format
                std::string strID(reinterpret_cast<char*>(&out[0]));
                strID.erase (strID.begin()+8, strID.end());

                // print results
                std::cout << "id = " << strID << std::endl;
				std::cout << "timestamp = " << out[1] << std::endl;
				std::cout << "lin_pos_x = " << out[2] << std::endl;
				std::cout << "lin_pos_y = " << out[3] << std::endl;
				std::cout << "lin_pos_z = " << out[4] << std::endl;
				std::cout << "lin_vel_x = " << out[5] << std::endl;
				std::cout << "lin_vel_y = " << out[6] << std::endl;
				std::cout << "lin_vel_z = " << out[7] << std::endl;
				std::cout << "lin_acc_x = " << out[8] << std::endl;
				std::cout << "lin_acc_y = " << out[9] << std::endl;
				std::cout << "lin_acc_z = " << out[10] << std::endl;
				std::cout << "quaternion_x = " << out[11] << std::endl;
				std::cout << "quaternion_y = " << out[12] << std::endl;
				std::cout << "quaternion_z = " << out[13] << std::endl;
				std::cout << "quaternion_w = " << out[14] << std::endl;
				std::cout << "ang_vel_x = " <<out[15] << std::endl;
				std::cout << "ang_vel_y = " <<out[16] << std::endl;
				std::cout << "ang_vel_z = " <<out[17] << std::endl;
				std::cout << "ang_acc_x = " <<out[18] << std::endl;
				std::cout << "ang_acc_y = " <<out[19] << std::endl;
				std::cout << "ang_acc_z = " <<out[20] << std::endl;
				std::cout << "button_1 = " << out[21] << std::endl;
				std::cout << "button_2 = " << out[22] << std::endl;
				std::cout << "Received datagram.\n" << std::endl;
			}
		}else{
		// failed. missing bytes.
		}
		// Clears buffer 
    	memset(&buffer, 0, BUFSIZE);
	}
}