%* ==============================================
% @author		Nemanja Babic
% @email 		nemanja.babic@hotmail.com
% @date  		May 2017
%================================================
% Socket Programming (UDP) - MATLAB
%================================================
% Description: UDP Client
%================================================ */

%------------------ BEGIN CODE -------------------

function client_udp

clc; % clear command window
fclose(instrfindall); % close all ports

%%%%%%%%%%%%%%%%%%%%%%%% VARIABLES %%%%%%%%%%%%%%%%%%%%%%%
syms controller_id % controller id
syms timestamp % timestamp from unity server

syms lin_pos_x lin_pos_y lin_pos_z % linear position
syms lin_vel_x lin_vel_y lin_vel_z % linear velocity
syms lin_acc_x lin_acc_y lin_acc_z % linear acceleration

syms quaternion_x quaternion_y quaternion_z quaternion_w % orientation in free space in quaternion form
syms ang_vel_x ang_vel_y ang_vel_z % angular velocity
syms ang_acc_x ang_acc_y ang_acc_z % angular acceleration

syms button_1 button_2 % button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Client UDP\n')

% 'L' for little-endian byte ordering or 'B' for big-endian byte ordering.
[str,maxsize,endian] = computer;

% 184 is the buffer size
BUFSIZE = 184;

% 5400 is the port number
PORT = 5400;

% create UDP object
% 127.0.0.1 is the loopback IP address also referred to as the “localhost”
% Timeout is set to infinity (forever waiting for package)
u = udp('127.0.0.1','LocalPort',PORT,'InputBufferSize',BUFSIZE,'Timeout',Inf);

% Allows other UDP sockets to bind to the UDP object's LocalPort
%u.EnablePortSharing = 'on';

% connect the UDP object
fopen(u);

% only way to terminate is by ctrl+c
% there should be a conditionnal statement to terminate smoothly, so that
% the UDP socket can close properly
% if you get error "Unsuccessful open: Address already in use: Cannot bind"
% run the following on the command line: >> fclose(instrfindall)
while true

try
    % receive package
    byte_vector = fread(u,1);
catch ME
    % disconnect the UDP object
    fclose(u);
    return
end

if strcmp('L',endian) 
    
    % LITTLE ENDIAN SYSTEM
    
    % convert byte vector to IEEE double vector
    double_vector =  typecast(uint8(byte_vector), 'double');

    % extract UDP package data individually
    
    % byte to ascii char 
    controller_id = strcat(native2unicode(byte_vector(1)),native2unicode(byte_vector(2)),native2unicode(byte_vector(3)),native2unicode(byte_vector(4)),native2unicode(byte_vector(5)),native2unicode(byte_vector(6)),native2unicode(byte_vector(7)),native2unicode(byte_vector(8)))
    
    timestamp = double_vector(2)
    
    lin_pos_x = double_vector(3)
    lin_pos_y = double_vector(4)
    lin_pos_z = double_vector(5)
    
    lin_vel_x = double_vector(6)
    lin_vel_y = double_vector(7)
    lin_vel_z = double_vector(8)
    
    lin_acc_x = double_vector(9)
    lin_acc_y = double_vector(10)
    lin_acc_z = double_vector(11)

    quaternion_x = double_vector(12)
    quaternion_y = double_vector(13)
    quaternion_z = double_vector(14)
    quaternion_w = double_vector(15)
    
    ang_vel_x = double_vector(16)
    ang_vel_y = double_vector(17)
    ang_vel_y = double_vector(18)
    
    ang_acc_x = double_vector(19)
    ang_acc_y = double_vector(20)
    ang_acc_z = double_vector(21)
	
	button_1 = double_vector(22)
	button_2 = double_vector(23)
    
    else
    
    % BIG ENDIAN SYSTEM
    
    % swap vector ordering
    byte_vector = byte_vector(end:-1:1);
    
    % convert byte vector to IEEE double vector
    double_vector =  typecast(uint8(byte_vector), 'double');

    % extract UDP package data individually
    
    % byte to ascii char 
    controller_id = strcat(native2unicode(byte_vector(1)),native2unicode(byte_vector(2)),native2unicode(byte_vector(3)),native2unicode(byte_vector(4)),native2unicode(byte_vector(5)),native2unicode(byte_vector(6)),native2unicode(byte_vector(7)),native2unicode(byte_vector(8)))
    
    timestamp = double_vector(2)
    
    lin_pos_x = double_vector(3)
    lin_pos_y = double_vector(4)
    lin_pos_z = double_vector(5)
    
    lin_vel_x = double_vector(6)
    lin_vel_y = double_vector(7)
    lin_vel_z = double_vector(8)
    
    lin_acc_x = double_vector(9)
    lin_acc_y = double_vector(10)
    lin_acc_z = double_vector(11)

    quaternion_x = double_vector(12)
    quaternion_y = double_vector(13)
    quaternion_z = double_vector(14)
    quaternion_w = double_vector(15)
    
    ang_vel_x = double_vector(16)
    ang_vel_y = double_vector(17)
    ang_vel_y = double_vector(18)
    
    ang_acc_x = double_vector(19)
    ang_acc_y = double_vector(20)
    ang_acc_z = double_vector(21)
	
	button_1 = double_vector(22)
	button_2 = double_vector(23)
end
end  
%------------------ END OF CODE -------------------