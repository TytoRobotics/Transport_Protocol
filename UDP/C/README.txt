===========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Socket Programming (UDP) C:
===========================================

Socket Programming User Datagram Protocol (UDP)

	-Server & Client Concept.

	-Major steps.

		-Client:
			=> Create a socket with the socket()
			=> Send and receive data, use the recvfrom() system calls

	-Client code: client_udp.c

	(Linux) compile c code		:	$ cc <file>.c -o <name>.out
	(Linux) run c code			:	$ ./<name>.out <arg1> <arg2> ...

	(Windows) compile c code  :	> cl <file_name>.c
	(Windows) run c code	  :	> <file_name>.exe