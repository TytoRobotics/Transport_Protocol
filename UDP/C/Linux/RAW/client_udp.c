// Creates a UDP server in the internet domain.

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

void error(char *);

int main(int argc, char *argv[]){

	int _socket, length, n;
	struct sockaddr_in server, from;
	struct hostent * hp;
	char buffer[256];

	if(argc != 3){
		printf("Usage: server port\n");
		exit(1);
	}

	_socket = socket(AF_INET, SOCK_DGRAM, 0);

	if(_socket < 0){
		error("Opening socket");
	}

	server.sin_family = AF_INET;
	hp = gethostbyname(argv[1]); // hostname (local host)

	if(hp == 0){
		error("Unknown host");
	}

	bcopy((char *)hp->h_addr, (char *)&server.sin_addr, hp->h_length); // copying address into "server" variable
	server.sin_port = htons(atoi(argv[2])); // getting port number
	length = sizeof(struct sockaddr_in);

	printf("Please enter the message: ");

	bzero(buffer, 256);	// clear buffer
	fgets(buffer, 255, stdin); // get input value from stdin and store in buffer

	n = sendto(_socket, buffer, strlen(buffer), 0, (struct sockaddr *) &server, length); // send to server

	if(n < 0){
		error("sendto");
	}

	bzero(buffer, 256);	

	n = recvfrom(_socket, buffer, 256, 0, (struct sockaddr *) &from, &length); // receive from server

	if(n < 0){
		error("recvfrom");
	}

	write(1,"Got feedback: ",14);
	write(1, buffer, n);
}
