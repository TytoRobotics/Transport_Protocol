// Creates a UDP server.
// The port number is passed as an argument.
// This server runs forever.

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> // data types for representing socket addresses in the Internet namespace are defined in the header filer netinet/in.h
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

// error method
void error(char *msg){
	perror(msg);
	exit(0);
}

// main program
int main(int argc, char *argv[]){
	
	int _socket, length, fromlen, n;
	struct sockaddr_in server; // struct sockaddr_in data type used to represent socket addresses in the Internet namespace
	struct sockaddr_in from;
	char buffer[1024];

	if(argc < 2){
		fprintf(stderr, "Error, no port provided\n");
		exit(0);
	}

	// AF_INET -> IP Concept (Internet Protocol v4)
	// SOCK_DGRAM -> UDP Concept
	_socket = socket(AF_INET, SOCK_DGRAM, 0);

	// Verifies if UDP socket created successfully
	if(_socket < 0){
		error("Opening socket");
	}

	// clearing the server
	length = sizeof(server);
	bzero(&server, length);

	// specify address family that is used for communication
	// can only use addresses of type "IPv4"
	server.sin_family = AF_INET; 

	//  allowes your program to work without knowing the IP address of the machine it is running on
	//  allowes your server to receive packets destined to any interface
	//  sin_addr is the Internet address of the host machine
	server.sin_addr.s_addr = INADDR_ANY; 

	// getting port number from argv[1]
	// converting port number from string to int using atoi method
	// converting port number from int to network understandable format 
	server.sin_port = htons(atoi(argv[1]));

	// binding _socket variable to server
	if(bind(_socket, (struct sockaddr *) &server, length) < 0){
		error("binding");
	}

	fromlen = sizeof(struct sockaddr_in);

	// server in infinite loop (for multiple user request)
	while(1){
		
		// recvfrom function reads one packet from the socket "_socket" into the buffer "buffer". 
		// the size argument specifies the maximum number of bytes to be read (here: 1024 bytes) 
		// if the packet is longer than size bytes, then you get the first size bytes of the packet 
		// and the rest of the packet is lost. There’s no way to read the rest of the packet. 
		// thus, when you use a packet protocol, you must always know how long a packet to expect.
		// the from and from-len arguments are used to return the address where the packet came from
		n = recvfrom(_socket, buffer, 1024, 0, (struct sockaddr *) &from, &fromlen);
	
		if(n < 0){
			error("recvfrom");
		}
	
		// writing "Received Datagram"
		write(1, "Received Datagram : ", 20);
		// writing content of buffer
		write(1, buffer, n);

		// sendto() is used for UDP SOCK_DGRAM unconnected datagram sockets. 
		// with the unconnected sockets, you must specify the destination of a packet each time you send one, 
		n = sendto(_socket, "Got your message\n", 17, 0, (struct sockaddr *) &from, fromlen);

		if(n < 0){
			error("sendto");
		}
	}
}
