/* ========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Linux Socket Programming (UDP) - C
===========================================
Description: UDP Client
=========================================== */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> // data types for representing socket addresses in the Internet namespace are defined in the header filer netinet/in.h
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 184
#define PORT 5400

typedef enum { false, true } bool;


// union udp_pkg used to read data as double type
union udp_pkg{
	double data[23];
	unsigned char buffer[BUFSIZE];
};

// error method
void error(char *msg){
	perror(msg);
	exit(0);
}

// checkNativeEndianness verifies Endianness
bool checkNativeEndianness(){
	unsigned short a=0x1234;
	if (*((unsigned char *)&a)==0x12)
		return BIG_ENDIAN;
	else
		return LITTLE_ENDIAN;
}


// printUdpPackage prints content of package
void printUdpPackage(union udp_pkg * ptr){
		
		// printing content of buffer
    
        // convert byte array to ascii character
        char id[8] = {(*ptr).buffer[0],(*ptr).buffer[1],(*ptr).buffer[2],(*ptr).buffer[3],(*ptr).buffer[4],(*ptr).buffer[5],(*ptr).buffer[6],(*ptr).buffer[7]};

        printf("id = %s\n",id);
    
        printf("timestamp = %f\n",(*ptr).data[1]);

		printf("lin_pos_x = %f\n",(*ptr).data[2]);
		printf("lin_pos_y = %f\n",(*ptr).data[3]);
		printf("lin_pos_z = %f\n",(*ptr).data[4]);
		
		printf("lin_vel_x = %f\n",(*ptr).data[5]);
		printf("lin_vel_y = %f\n",(*ptr).data[6]);
		printf("lin_vel_z = %f\n",(*ptr).data[7]);
		
		printf("lin_acc_x = %f\n",(*ptr).data[8]);
		printf("lin_acc_y = %f\n",(*ptr).data[9]);
		printf("lin_acc_z = %f\n",(*ptr).data[10]);
			
		printf("quaternion_x = %f\n",(*ptr).data[11]);
		printf("quaternion_y = %f\n",(*ptr).data[12]);
		printf("quaternion_z = %f\n",(*ptr).data[13]);
		printf("quaternion_w = %f\n",(*ptr).data[14]);
		
		printf("ang_vel_x = %f\n",(*ptr).data[15]);
		printf("ang_vel_y = %f\n",(*ptr).data[16]);
		printf("ang_vel_z = %f\n",(*ptr).data[17]);
		
		printf("ang_acc_x = %f\n",(*ptr).data[18]);
		printf("ang_acc_y = %f\n",(*ptr).data[19]);
		printf("ang_acc_z = %f\n",(*ptr).data[20]);

		printf("button_1 = %f\n",(*ptr).data[21]);
		printf("button_2 = %f\n\n",(*ptr).data[22]);
		}

// main program
int main(int argc, char *argv[]){

	int _socket, length, fromlen, n;
	struct sockaddr_in client; // struct sockaddr_in data type used to represent socket addresses in the Internet namespace
	struct sockaddr_in from;
	union udp_pkg udp_pkg;

	bool little_endian = checkNativeEndianness(); // check endianness

	// AF_INET -> IP Concept (Internet Protocol v4)
	// SOCK_DGRAM -> UDP Concept
	_socket = socket(AF_INET, SOCK_DGRAM, 0);

	// Verifies if UDP socket created successfully
	if(_socket < 0){
		error("Opening socket");
	}

	// clearing the client
	length = sizeof(client);
	bzero(&client, length);

	// specify address family that is used for communication
	// can only use addresses of type "IPv4"
	client.sin_family = AF_INET; 

	// allowes your program to work without knowing the IP address of the machine it is running on
	// allowes your server to receive packets destined to any interface
	// sin_addr is the Internet address of the host machine
	client.sin_addr.s_addr = INADDR_ANY; 

	// getting port number from argv[1]
	// converting port number from string to int using atoi method
	// converting port number from int to network understandable format 
	client.sin_port = htons(PORT);

	// binding _socket variable to client
	if(bind(_socket, (struct sockaddr *) &client, length) < 0){
		error("binding");
	}

	fromlen = sizeof(struct sockaddr_in);

	// client in infinite loop (for multiple user request)
	while(1){
		
		write(1, "Waiting for Datagram ... \n", 27);	

		// recvfrom function reads one packet from the socket "_socket" into the buffer "buffer". 
		// the size argument specifies the maximum number of bytes to be read (here: 1024 bytes) 
		// if the packet is longer than size bytes, then you get the first size bytes of the packet 
		// and the rest of the packet is lost. There’s no way to read the rest of the packet. 
		// thus, when you use a packet protocol, you must always know how long a packet to expect.
		// the from and from-len arguments are used to return the address where the packet came from
		n = recvfrom(_socket, udp_pkg.buffer, BUFSIZE, 0, (struct sockaddr *) &from, &fromlen);
	
		if(n < 0){
			error("recvfrom");
		}
	
		// writing "Received Datagram"
		printf("Received Datagram. \n");

		if(little_endian){

			// little endian system

			// print content
			printUdpPackage(&udp_pkg);

		}else{

			// big endian system
		
			// swap byte-order for big-endian system
			for (int i=0; i<(sizeof udp_pkg)/2; i++)
	    	{
	        	char temp = udp_pkg.buffer[i];
	        	udp_pkg.buffer[i] = udp_pkg.buffer[(sizeof udp_pkg)-1-i];
	        	udp_pkg.buffer[(sizeof udp_pkg)-1-i] = temp;
	    	}

	    	// print content
	    	printUdpPackage(&udp_pkg);
		}

		// debug section
		// sendto() is used for UDP SOCK_DGRAM unconnected datagram sockets. 
		// with the unconnected sockets, you must specify the destination of a packet each time you send one, 
		//n = sendto(_socket, "Got your message\n", 17, 0, (struct sockaddr *) &from, fromlen);

		//if(n < 0){
		//	error("sendto");
		//}
	}
}
