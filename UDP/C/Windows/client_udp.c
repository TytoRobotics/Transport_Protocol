/* ========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Windows Socket Programming (UDP) - C
===========================================
Description: UDP Client
=========================================== */
 
#include<stdio.h>
#include<winsock2.h>
 
#pragma comment(lib,"ws2_32.lib") //Winsock Library
 
#define BUFSIZE 184  //Max length of buffer
#define PORT 5400   //The port on which to listen for incoming data

typedef enum { false, true } bool;

// union udp_pkg used to read data as double type
union udp_pkg{
	double data[23];
	unsigned char buffer[BUFSIZE];
};

// checkNativeEndianness verifies Endianness
bool checkNativeEndianness(){
	unsigned short a=0x1234;
	if (*((unsigned char *)&a)==0x12)
		return false; // big endian
	else
		return true; // little endian
}

// printUdpPackage prints content of package
void printUdpPackage(union udp_pkg * ptr){
		
		// printing content of buffer
    
        // convert byte array to ascii character
        char id[9] = {(*ptr).buffer[0],(*ptr).buffer[1],(*ptr).buffer[2],(*ptr).buffer[3],(*ptr).buffer[4],(*ptr).buffer[5],(*ptr).buffer[6],(*ptr).buffer[7], NULL};

        printf("id = %s\n",id);
    
        printf("timestamp = %f\n",(*ptr).data[1]);
		printf("timestamp = %f\n",(*ptr).data[1]);

		printf("lin_pos_x = %f\n",(*ptr).data[2]);
		printf("lin_pos_y = %f\n",(*ptr).data[3]);
		printf("lin_pos_z = %f\n",(*ptr).data[4]);
		
		printf("lin_vel_x = %f\n",(*ptr).data[5]);
		printf("lin_vel_y = %f\n",(*ptr).data[6]);
		printf("lin_vel_z = %f\n",(*ptr).data[7]);
		
		printf("lin_acc_x = %f\n",(*ptr).data[8]);
		printf("lin_acc_y = %f\n",(*ptr).data[9]);
		printf("lin_acc_z = %f\n",(*ptr).data[10]);
			
		printf("quaternion_x = %f\n",(*ptr).data[11]);
		printf("quaternion_y = %f\n",(*ptr).data[12]);
		printf("quaternion_z = %f\n",(*ptr).data[13]);
		printf("quaternion_w = %f\n",(*ptr).data[14]);
		
		printf("ang_vel_x = %f\n",(*ptr).data[15]);
		printf("ang_vel_y = %f\n",(*ptr).data[16]);
		printf("ang_vel_z = %f\n",(*ptr).data[17]);
		
		printf("ang_acc_x = %f\n",(*ptr).data[18]);
		printf("ang_acc_y = %f\n",(*ptr).data[19]);
		printf("ang_acc_z = %f\n",(*ptr).data[20]);

		printf("button_1 = %f\n", (*ptr).data[21]);
		printf("button_2 = %f\n\n", (*ptr).data[22]);

}
 
int main()
{
    SOCKET s;
    struct sockaddr_in client, si_other;
    int slen , recv_len;
    WSADATA wsa;
 
    slen = sizeof(si_other) ;

    union udp_pkg udp_pkg;

	bool little_endian = checkNativeEndianness(); // check endianness
     
    //Initialise winsock
    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
    {
        printf("Failed. Error Code : %d",WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    printf("Initialised.\n");
     
    //Create a socket
    if((s = socket(AF_INET , SOCK_DGRAM , 0 )) == INVALID_SOCKET)
    {
        printf("Could not create socket : %d" , WSAGetLastError());
    }
    printf("Socket created.\n");
     
    //Prepare the sockaddr_in structure
    client.sin_family = AF_INET;
    client.sin_addr.s_addr = INADDR_ANY;
    client.sin_port = htons( PORT );
     
    //Bind
    if( bind(s ,(struct sockaddr *)&client , sizeof(client)) == SOCKET_ERROR)
    {
        printf("Bind failed with error code : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    puts("Bind done");
 
    //keep listening for data
    while(1)
    {
        printf("Waiting for Datagram...\n");
        fflush(stdout);
         
        //clear the buffer by filling null, it might have previously received data
        memset(udp_pkg.buffer,'\0', BUFSIZE);
         
        //try to receive some data, this is a blocking call
        if ((recv_len = recvfrom(s, udp_pkg.buffer, BUFSIZE, 0, (struct sockaddr *) &si_other, &slen)) == SOCKET_ERROR)
        {
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            exit(EXIT_FAILURE);
        }
         
        //print details of the client/peer and the data received
        printf("Received datagram from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));

        if(little_endian){
        	// little endian system

			// print content
			printUdpPackage(&udp_pkg);
        }
        else{
        	// big endian system
		
			// swap byte-order for big-endian system
			for (int i=0; i<(sizeof udp_pkg)/2; i++)
	    	{
	        	char temp = udp_pkg.buffer[i];
	        	udp_pkg.buffer[i] = udp_pkg.buffer[(sizeof udp_pkg)-1-i];
	        	udp_pkg.buffer[(sizeof udp_pkg)-1-i] = temp;
	    	}

	    	// print content
	    	printUdpPackage(&udp_pkg);
        }
    }
 
    closesocket(s);
    WSACleanup();
     
    return 0;
}