#!/usr/bin/python
# -*- coding: utf-8 -*-

''' 
==========================================
@author........Nemanja Babic
@email ........nemanja.babic@hotmail.com
@date  ........May 2017
==========================================
Socket Programming (UDP) - Python
==========================================
Description: UDP Client
==========================================
'''

import sys
import socket
import array
import struct


class RCbenchmark_pkg:

    # Empty constructor
    def __init__(self):
        pass

    # Function to update data
    def update_data(self, doubles_sequence):
        self.id = doubles_sequence[0]
        self.timestamp = doubles_sequence[1]
        self.lin_pos_x = doubles_sequence[2]
        self.lin_pos_y = doubles_sequence[3]
        self.lin_pos_z = doubles_sequence[4]
        self.lin_vel_x = doubles_sequence[5]
        self.lin_vel_y = doubles_sequence[6]
        self.lin_vel_z = doubles_sequence[7]
        self.lin_acc_x = doubles_sequence[8]
        self.lin_acc_y = doubles_sequence[9]
        self.lin_acc_z = doubles_sequence[10]
        self.quaternion_x = doubles_sequence[11]
        self.quaternion_y = doubles_sequence[12]
        self.quaternion_z = doubles_sequence[13]
        self.quaternion_w = doubles_sequence[14]
        self.ang_vel_x = doubles_sequence[15]
        self.ang_vel_y = doubles_sequence[16]
        self.ang_vel_z = doubles_sequence[17]
        self.ang_acc_x = doubles_sequence[18]
        self.ang_acc_y = doubles_sequence[19]
        self.ang_acc_z = doubles_sequence[20]
        self.button_1 = doubles_sequence[21]
        self.button_2 = doubles_sequence[22]
        
        self.id = bytearray(struct.pack("d", self.id)).decode('ascii')

    # Function to print content of UDP package sent by server
    def print_udp_pkg(self):
        s_unicode = bytearray(struct.pack("d", self.id)).decode('ascii')
        # print([ "0x%02x" % b for b in ba ])
        print ("id : " + s_unicode)
        print ("timestamp : %f" % self.timestamp)
        print ("lin_pos_x : %f" % self.lin_pos_x)
        print ("lin_pos_y : %f" % self.lin_pos_y)
        print ("lin_pos_z : %f" % self.lin_pos_z)
        print ("lin_vel_x : %f" % self.lin_vel_x)
        print ("lin_vel_y : %f" % self.lin_vel_y)
        print ("lin_vel_z : %f" % self.lin_vel_z)
        print ("lin_acc_x : %f" % self.lin_acc_x)
        print ("lin_acc_y : %f" % self.lin_acc_y)
        print ("lin_acc_z : %f" % self.lin_acc_z)
        print ("quaternion_x : %f" % self.quaternion_x)
        print ("quaternion_y : %f" % self.quaternion_y)
        print ("quaternion_z : %f" % self.quaternion_z)
        print ("quaternion_w : %f" % self.quaternion_w)
        print ("ang_vel_x : %f" % self.ang_vel_x)
        print ("ang_vel_y : %f" % self.ang_vel_y)
        print ("ang_vel_z : %f" % self.ang_vel_z)
        print ("ang_acc_x : %f" % self.ang_acc_x)
        print ("ang_acc_y : %f" % self.ang_acc_y)
        print ("ang_acc_z : %f" % self.ang_acc_z)
        print ("button_1 : %f" % self.button_1)
        print ("button_2 : %f" % self.button_2)


def main():

    # IP Address - on Linux verify by typing : $ hostname -I
    # 127.0.0.1 is the localhost loopback address

    UDP_IP = '127.0.0.1'

    BUFSIZE = 184  # buffer size is 184 bytes

    UDP_PORT = 5400  # Port number is 5400

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # Internet. UDP.

    sock.bind((UDP_IP, UDP_PORT))  # bind socket

    rcbenchmark_pkg = RCbenchmark_pkg()  # empty object

    while True:
        print('Waiting for datagram ...')
        (data, addr) = sock.recvfrom(BUFSIZE)  # buffer size is 184 bytes
        doubles_sequence = array.array('d', data)
        rcbenchmark_pkg.update_data(doubles_sequence)
        if sys.byteorder == 'little':
            rcbenchmark_pkg.print_udp_pkg()
        else:

              # "big"

            doubles_sequence.byteswap()  # swap byte order to match little-endian
            rcbenchmark_pkg.print_udp_pkg()
        print('Received datagram.\n')


if __name__ == '__main__':
    main()

