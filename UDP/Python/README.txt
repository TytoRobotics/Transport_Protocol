===========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Socket Programming (UDP) Python:
===========================================

Socket Programming User Datagram Protocol (UDP)

	-Server & Client Concept.

	-Major steps.
		-Client:
			=> Create a socket with the socket()
			=> Receive data, use the recvfrom() system calls

	-Client code: client_udp.py

	(linux and windows) run python code	:	$ python client_udp.py