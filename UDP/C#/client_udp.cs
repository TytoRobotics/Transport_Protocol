/* ========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Socket Programming (UDP) - C#
===========================================
Description: UDP Client
=========================================== */

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class RCbenchmark_pkg 
{  
   	public double id, timestamp;

	public double lin_pos_x, lin_pos_y, lin_pos_z;
	public double lin_vel_x, lin_vel_y, lin_vel_z;
	public double lin_acc_x, lin_acc_y, lin_acc_z;

	public double quaternion_x, quaternion_y, quaternion_z,quaternion_w;
	public double ang_vel_x, ang_vel_y, ang_vel_z;
	public double ang_acc_x, ang_acc_y, ang_acc_z;

	public double button_1, button_2;

	public RCbenchmark_pkg(){}

	public void update_data(byte[] receive_byte_array){
		
		id 				= BitConverter.ToDouble(receive_byte_array,0);
		timestamp 		= BitConverter.ToDouble(receive_byte_array,8);
		lin_pos_x 		= BitConverter.ToDouble(receive_byte_array,16);
		lin_pos_y 		= BitConverter.ToDouble(receive_byte_array,24);
		lin_pos_z 		= BitConverter.ToDouble(receive_byte_array,32);
		lin_vel_x 		= BitConverter.ToDouble(receive_byte_array,40);
		lin_vel_y 		= BitConverter.ToDouble(receive_byte_array,48);
		lin_vel_z 		= BitConverter.ToDouble(receive_byte_array,56);
		lin_acc_x 		= BitConverter.ToDouble(receive_byte_array,64);
		lin_acc_y 		= BitConverter.ToDouble(receive_byte_array,72);
		lin_acc_z 		= BitConverter.ToDouble(receive_byte_array,80);
		quaternion_x	= BitConverter.ToDouble(receive_byte_array,88);
		quaternion_y 	= BitConverter.ToDouble(receive_byte_array,96);
		quaternion_z 	= BitConverter.ToDouble(receive_byte_array,104);
		quaternion_w 	= BitConverter.ToDouble(receive_byte_array,112);
		ang_vel_x 		= BitConverter.ToDouble(receive_byte_array,120);
		ang_vel_y 		= BitConverter.ToDouble(receive_byte_array,128);
		ang_vel_z 		= BitConverter.ToDouble(receive_byte_array,136);
		ang_acc_x 		= BitConverter.ToDouble(receive_byte_array,144);
		ang_acc_y 		= BitConverter.ToDouble(receive_byte_array,152);
		ang_acc_z 		= BitConverter.ToDouble(receive_byte_array,160);
		button_1		= BitConverter.ToDouble(receive_byte_array,168);
		button_2		= BitConverter.ToDouble(receive_byte_array,176);
	
	}
}  

public class UDPClient{

	// Client will listen on port 5400
	private const int clientPort = 5400;

	public static int Main(){

		// Create an object of type RCbenchmark_pkg
        RCbenchmark_pkg rcbenchmark_pkg = new RCbenchmark_pkg();
		
		// Initializes a new instance of the UdpClient class and binds it to the local port number provided
		UdpClient listener = new UdpClient(clientPort);
		
		// Represents a network endpoint as an IP address and a port number
		IPEndPoint netEndpoint = new IPEndPoint(IPAddress.Any, clientPort);
		
		// UDP package of server will be stored here
		byte[] receive_byte_array;

		bool done = false;

		try{
			while (!done){

				Console.WriteLine("Waiting for datagram");
				receive_byte_array = listener.Receive(ref netEndpoint);
				Console.WriteLine("Received a datagram from {0}", netEndpoint.ToString() );

				if(BitConverter.IsLittleEndian)
				{	
					// Little Endian

					// Update data
					rcbenchmark_pkg.update_data(receive_byte_array);
				}
				else
				{
					
					// Big Endian

					// Function to swap byte-order for big-endian system
					for (int i = 0; i < receive_byte_array.Length / 2; i++)
					{
				 	  byte tmp = receive_byte_array[i];
				 	  receive_byte_array[i] = receive_byte_array[receive_byte_array.Length - i - 1];
				 	  receive_byte_array[receive_byte_array.Length - i - 1] = tmp;
					}

					// Update data
					rcbenchmark_pkg.update_data(receive_byte_array);
				}
                
                byte[] encodedBytesFromDouble = BitConverter.GetBytes(rcbenchmark_pkg.id);
                string s_unicode = System.Text.Encoding.ASCII.GetString(encodedBytesFromDouble);
                
				Console.WriteLine("Controller ID = {0}", s_unicode);
				Console.WriteLine("Time Stamp = {0} s", rcbenchmark_pkg.timestamp);
				Console.WriteLine("lin_pos_x = {0}, lin_pos_y = {1}, lin_pos_z = {2}", rcbenchmark_pkg.lin_pos_x, rcbenchmark_pkg.lin_pos_y, rcbenchmark_pkg.lin_pos_z);
				Console.WriteLine("lin_vel_x = {0}, lin_vel_x = {1}, lin_vel_z = {2}", rcbenchmark_pkg.lin_vel_x, rcbenchmark_pkg.lin_vel_y, rcbenchmark_pkg.lin_vel_z);
				Console.WriteLine("lin_acc_x = {0}, lin_acc_y = {1}, lin_acc_z = {2}", rcbenchmark_pkg.lin_acc_x, rcbenchmark_pkg.lin_acc_y, rcbenchmark_pkg.lin_acc_z);
				Console.WriteLine("quaternion_x = {0}, quaternion_y = {1}, quaternion_z = {2}, quaternion_w = {3}", rcbenchmark_pkg.quaternion_x, rcbenchmark_pkg.quaternion_y, rcbenchmark_pkg.quaternion_z, rcbenchmark_pkg.quaternion_w );
				Console.WriteLine("ang_vel_x = {0}, ang_vel_x = {1}, ang_vel_z = {2}", rcbenchmark_pkg.ang_vel_x, rcbenchmark_pkg.ang_vel_y, rcbenchmark_pkg.ang_vel_z);
				Console.WriteLine("ang_acc_x = {0}, ang_acc_y = {1}, ang_acc_z = {2}", rcbenchmark_pkg.ang_acc_x, rcbenchmark_pkg.ang_acc_y, rcbenchmark_pkg.ang_acc_z);
				Console.WriteLine("button_1 = {0}, button_2 = {1} \n", rcbenchmark_pkg.button_1,rcbenchmark_pkg.button_2);
				}
			}catch (Exception e){
				Console.WriteLine(e.ToString());
				}
				listener.Close();
				return 0;
			}
} // end of class UDPClient
