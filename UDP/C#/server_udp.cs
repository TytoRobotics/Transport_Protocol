/* ========================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
===========================================
Socket Programming (UDP) - C#
===========================================
Description: UDP Server
=========================================== */

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections;

public struct UDPPackage{

	// controller id
	public double id;

	// time stamp
	public double timestamp;

	// linear position data
	public double lin_pos_x;
	public double lin_pos_y;
	public double lin_pos_z;

	// linear velocity data
	public double lin_vel_x;
	public double lin_vel_y;
	public double lin_vel_z;

	// linear acceleration data
	public double lin_acc_x;
	public double lin_acc_y;
	public double lin_acc_z;

	// orientation in free space in quaternion form
	public double quaternion_x;
	public double quaternion_y;
	public double quaternion_z;
	public double quaternion_w;

	// angular velocity data
	public double ang_vel_x;
	public double ang_vel_y;
	public double ang_vel_z;

	// angular acceleration data
	public double ang_acc_x;
	public double ang_acc_y;
	public double ang_acc_z;

	// button
	public double button_1, button_2;
}

class UDPServer{

	static byte[] getBytes(UDPPackage str) {
	    int size = Marshal.SizeOf(str);
	    byte[] arr = new byte[size];
	    IntPtr ptr = Marshal.AllocHGlobal(size);
	    Marshal.StructureToPtr(str, ptr, true);
	    Marshal.Copy(ptr, arr, 0, size);
	    Marshal.FreeHGlobal(ptr);
	    return arr;
	}

	static void PrintByteArray(byte[] bytes){
	    var sb = new StringBuilder("new byte[] { ");
    	foreach (var b in bytes){
        sb.Append(b + ", ");
    	}
    	sb.Append("}");
    	Console.WriteLine(sb.ToString());
	}

	static void Main(string[] args){

        Boolean exception_thrown = false;

        // Initializes a new instance of the Socket class using the specified address family, socket type and protocol
        Socket sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        // Provides an Internet Protocol (IP) address
        IPAddress send_to_address = IPAddress.Parse("127.0.0.1");

        // Sending to IP address with specified port number
        IPEndPoint sending_end_point = new IPEndPoint(send_to_address, 5400);

        Console.WriteLine("The data to broadcast via UDP is ...");

        // ***** Test Package *****
        UDPPackage pkg;

        // controller serial key
        string unicodeString = "0123ABCD";
        //Console.WriteLine("Serial Key of Controller: {0}",unicodeString);

        // The encoding.
        ASCIIEncoding ascii = new ASCIIEncoding();

        // Encode string.
        Byte[] encodedBytes = ascii.GetBytes(unicodeString);

        //Console.WriteLine("ASCII Represenation of Serial Key:");
        //PrintByteArray(encodedBytes);

        // convert to double representation
        pkg.id = BitConverter.ToDouble(encodedBytes, 0);

        pkg.timestamp = 6.4123098;

        pkg.lin_pos_x = 1.1;
        pkg.lin_pos_y = 2.2;
        pkg.lin_pos_z = 3.3;

        pkg.lin_vel_x = 4.4;
        pkg.lin_vel_y = 5.5;
        pkg.lin_vel_z = 6.6;

        pkg.lin_acc_x = 0.0;
        pkg.lin_acc_y = 0.0;
        pkg.lin_acc_z = 0.0;

        pkg.quaternion_x = 10.10;
        pkg.quaternion_y = 11.11;
        pkg.quaternion_z = 12.12;
        pkg.quaternion_w = 13.13;

        pkg.ang_vel_x = 0.0;
        pkg.ang_vel_y = 0.0;
        pkg.ang_vel_z = 0.0;

        pkg.ang_acc_x = 17.17;
        pkg.ang_acc_y = 18.18;
        pkg.ang_acc_z = 19.19;

        pkg.button_1 = 1.0;
        pkg.button_2 = 2.0;
        // ************************

        Console.WriteLine("Controller ID = {0}", unicodeString);
        Console.WriteLine("lin_pos_x = {0}, lin_pos_y = {1}, lin_pos_z = {2}", pkg.lin_pos_x, pkg.lin_pos_y, pkg.lin_pos_z);
        Console.WriteLine("lin_vel_x = {0}, lin_vel_x = {1}, lin_vel_z = {2}", pkg.lin_vel_x, pkg.lin_vel_y, pkg.lin_vel_z);
        Console.WriteLine("lin_acc_x = {0}, lin_acc_y = {1}, lin_acc_z = {2}", pkg.lin_acc_x, pkg.lin_acc_y, pkg.lin_acc_z);

        Console.WriteLine("quaternion_x = {0}, quaternion_y = {1}, quaternion_z = {2}, quaternion_w = {3}", pkg.quaternion_x, pkg.quaternion_y, pkg.quaternion_z, pkg.quaternion_w );
        Console.WriteLine("ang_vel_x = {0}, ang_vel_x = {1}, ang_vel_z = {2}", pkg.ang_vel_x, pkg.ang_vel_y, pkg.ang_vel_z);
        Console.WriteLine("ang_acc_x = {0}, ang_acc_y = {1}, ang_acc_z = {2}", pkg.ang_acc_x, pkg.ang_acc_y, pkg.ang_acc_z);
        Console.WriteLine("button_1 = {0}, button_2 = {1}", pkg.button_1, pkg.button_2);

        byte[] send_buffer = getBytes(pkg);

        // PrintByteArray(send_buffer); // debug
        try{
                sending_socket.SendTo(send_buffer, sending_end_point);
                }catch (Exception send_exception ){
                    exception_thrown = true;
                    Console.WriteLine(" Exception {0}", send_exception.Message);
                }
                    if (exception_thrown == false){
                        Console.WriteLine("Message has been sent to the broadcast address");
                    }
                        else{
                            exception_thrown = false;
                            Console.WriteLine("The exception indicates the message was not sent.");
                        }
    } // end of main()
} // end of class Program
