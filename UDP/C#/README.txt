============================================
@author		Nemanja Babic
@email 		nemanja.babic@hotmail.com
@date  		May 2017
============================================
Socket Programming (UDP) C#:
============================================

Socket Programming User Datagram Protocol (UDP)

	-Server & Client Concept.

	-Major steps.
		-Server:
			=> Create a socket with the Socket()
			=> Provide IP:Port endpoint with the IPEndPoint()
			=> Send data, use SendTo()

		-Client:
			=> Create a socket and bind it to port with the UdpClient()
			=> Receive data, use the Receive()

	-Server code: server_udp.cs
	-Client code: client_udp.cs

	(linux & windows) compile c# code	:	$ mcs <file>.cs
	(linux & windows) run c# code		:	$ mono <file>.exe <port#>